import sys, csv, dns, dns.resolver, os
from mysql.connector import MySQLConnection, Error
from connect_mysql import connect
from mysql_dbconfig import path_to_data
from mysql_dbconfig import path_to_data_folder
from os import path


def insert_census(conn, values, table_name):
    if values is None:
        print("Values is empty")
        sys.exit(0)
    if conn is None or not(conn.is_connected()):
        print("Connection is not available.")
        sys.exit(0)
    cursor = conn.cursor()
    queryInsert = "insert ignore into {} (SUMLEV,REGION,DIVISION,STATE,NAME,CENSUS2010POP,ESTIMATESBASE2010,POPESTIMATE2010,POPESTIMATE2011,POPESTIMATE2012,POPESTIMATE2013,POPESTIMATE2014,POPESTIMATE2015,POPESTIMATE2016,NPOPCHG_2010,NPOPCHG_2011,NPOPCHG_2012,NPOPCHG_2013,NPOPCHG_2014,NPOPCHG_2015,NPOPCHG_2016,BIRTHS2010,BIRTHS2011,BIRTHS2012,BIRTHS2013,BIRTHS2014,BIRTHS2015,BIRTHS2016,DEATHS2010,DEATHS2011,DEATHS2012,DEATHS2013,DEATHS2014,DEATHS2015,DEATHS2016,NATURALINC2010,NATURALINC2011,NATURALINC2012,NATURALINC2013,NATURALINC2014,NATURALINC2015,NATURALINC2016,INTERNATIONALMIG2010,INTERNATIONALMIG2011,INTERNATIONALMIG2012,INTERNATIONALMIG2013,INTERNATIONALMIG2014,INTERNATIONALMIG2015,INTERNATIONALMIG2016,DOMESTICMIG2010,DOMESTICMIG2011,DOMESTICMIG2012,DOMESTICMIG2013,DOMESTICMIG2014,DOMESTICMIG2015,DOMESTICMIG2016,NETMIG2010,NETMIG2011,NETMIG2012,NETMIG2013,NETMIG2014,NETMIG2015,NETMIG2016,RESIDUAL2010,RESIDUAL2011,RESIDUAL2012,RESIDUAL2013,RESIDUAL2014,RESIDUAL2015,RESIDUAL2016,RBIRTH2011,RBIRTH2012,RBIRTH2013,RBIRTH2014,RBIRTH2015,RBIRTH2016,RDEATH2011,RDEATH2012,RDEATH2013,RDEATH2014,RDEATH2015,RDEATH2016,RNATURALINC2011,RNATURALINC2012,RNATURALINC2013,RNATURALINC2014,RNATURALINC2015,RNATURALINC2016,RINTERNATIONALMIG2011,RINTERNATIONALMIG2012,RINTERNATIONALMIG2013,RINTERNATIONALMIG2014,RINTERNATIONALMIG2015,RINTERNATIONALMIG2016,RDOMESTICMIG2011,RDOMESTICMIG2012,RDOMESTICMIG2013,RDOMESTICMIG2014,RDOMESTICMIG2015,RDOMESTICMIG2016,RNETMIG2011,RNETMIG2012,RNETMIG2013,RNETMIG2014,RNETMIG2015,RNETMIG2016)".format(table_name)
    queryValues = " values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    query = queryInsert + queryValues
    try:
        cursor.executemany(query, values)
        try:
            conn.commit()
        except:
            conn.rollback()
    except:
        cursor.execute(query, values)
        try:
            conn.commit()
        except:
            conn.rollback()
    cursor.close()
    print("Data inserted to table.")

def query_data(conn, table_name):
    if conn is None or not(conn.is_connected()):
        print("Connection is not available.")
        sys.exit(0)
    cursor = conn.cursor()
    try:
        query = "select min(POPESTIMATE2014) as min2014 from {} where POPESTIMATE2014 is not null".format(table_name)
        cursor.execute(query)
        record = cursor.fetchone()
        if record is not None:
            print("Min value for POPESTIMATE2014: ", record[0])

        query = "select max(POPESTIMATE2013) as max2013 from {} where POPESTIMATE2013 is not null".format(table_name)
        cursor.execute(query)
        record = cursor.fetchone()
        if record is not None:
            print("Max value for POPESTIMATE2013: ", record[0])

        query = "select avg(POPESTIMATE2012) as avg2012, stddev(POPESTIMATE2012) as stddev2012 from {} where POPESTIMATE2012 is not null".format(table_name)
        cursor.execute(query)
        record = cursor.fetchone()
        if record is not None:
            print("Mean value for POPESTIMATE2012: {} (+/- {})".format(record[0], record[1]))
    except Error as error:
        print(error)
    finally:
        cursor.close()

def main():
    conn = None
    tbl_name = "synergis.census"
    if len(sys.argv) == 2:
        tbl_name = sys.argv[1]
    elif len(sys.argv) == 3:
        tbl_name = sys.argv[1]
        path_to_data = path.join(path_to_data_folder, sys.argv[2])
    try:
        conn = connect()
        if conn is None or not (conn.is_connected()):
            print("Connection is not available.")
            sys.exit(0)

        with open(path_to_data, newline='') as census:
            data = []
            values = []
            print("Now processing nst-est2016-alldata.csv")
            reader = csv.reader(census, delimiter=",", dialect='excel')
            next(reader, None)
            for row in reader:
                try:
                    data = row
                    for i in range(0, len(data)):
                        data[i] = data[i].replace("'", "\"")
                    values.append(data)
                except Error as error:
                    print(error)
            insert_census(conn, values, tbl_name)
            query_data(conn, tbl_name)
            print("All done.")

    except Error as error:
        print(error)

    finally:
        if conn is not None and conn.is_connected():
            conn.close()
        sys.exit(0)

if __name__ == '__main__':
    main()
