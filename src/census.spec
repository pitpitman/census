# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

added_files = [
    ( 'dbconfig.ini', '.' ),
    ( 'nst-est2016-alldata.csv', '.')
]

a = Analysis(['census.py'],
             pathex=['C:\\Users\\Peter\\workspace_idea\\synergis\\src'],
             binaries=[],
             datas=added_files,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='pop_db_processor',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True )
