from configparser import ConfigParser
from os import path
import sys, os

# bundle_dir = getattr(sys, '_MEIPASS', path.abspath(path.dirname(__file__)))
# path_to_ini = path.join(bundle_dir, 'dbconfig.ini')
path_to_ini = path.join(path.dirname(__file__), 'dbconfig.ini')
path_to_data = path.join(path.dirname(__file__), 'nst-est2016-alldata.csv')
path_to_data_folder = path.dirname(__file__)

# dir_list = os.listdir(bundle_dir)
# print(dir_list)

def read_db_config(filename=path_to_ini, section='mysql'):
    parser = ConfigParser()
    parser.read(filename)
    db = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db[item[0]] = item[1]
    else:
        raise Exception('{0} not found in the {1} file'.format(section, filename))
    return db